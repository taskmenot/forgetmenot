class CreateJournals < ActiveRecord::Migration[6.1]
  def change
    create_table :journals do |t|
      t.string :icon
      t.string :title
      t.references :entry, null: false, foreign_key: true
      t.boolean :is_private
      t.string :color
      t.reference :author

      t.timestamps
    end
  end
end
