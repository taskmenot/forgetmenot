class CreateEntries < ActiveRecord::Migration[6.1]
  def change
    create_table :entries do |t|
      t.string :icon
      t.string :title
      t.string :content

      t.timestamps
    end
  end
end
