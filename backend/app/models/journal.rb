class Journal < ApplicationRecord
  has_and_belongs_to_many :entry
  belongs_to :user
end
