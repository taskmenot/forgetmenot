interface Author {
  firstName: string;
  lastName: string;
}

interface Entry {
  icon?: React.ReactNode;
  title: string;
  content: string;
}

interface Journal {
  icon?: React.ReactNode;
  title: string;
  entries: Entry[];
  isPrivate: boolean;
  color?: string;
  author: Author;
}
