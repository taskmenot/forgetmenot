import { TextField, Stack } from "@mui/material";
import JoditEditor from "jodit-react";
import React from "react";

interface EditorProps {
  entry: Entry;
  setEntry: (entry: Entry) => void;
}
const Editor: React.FC<EditorProps> = ({ entry, setEntry }) => {
  return (
    <Stack direction="column" gap="0.5rem">
      <TextField
        size="small"
        label="Journal Title"
        variant="outlined"
        value={entry.title}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setEntry({ ...entry, title: e.target.value })
        }
      />
      <JoditEditor
        value={entry.content}
        onChange={(value: string) => setEntry({ ...entry, content: value })}
      />
    </Stack>
  );
};

export default Editor;
