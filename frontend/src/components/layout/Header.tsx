import React from "react";
import { Link } from "react-router-dom";
import { Avatar, Button, Stack, Typography } from "@mui/material";

interface HeaderProps {}
const Header: React.FC<HeaderProps> = () => {
  return (
    <Stack
      direction="row"
      justifyContent="space-between"
      alignItems="center"
      width="100%"
      padding="1rem"
      gap="1rem"
      boxSizing="border-box"
    >
      <Stack
        direction="row"
        justifyContent="start"
        alignItems="center"
        gap="1rem"
      >
        <Typography variant="h6" noWrap>
          ForgetMeNot
        </Typography>
        <Button to="/" component={Link}>
          Home
        </Button>
        <Button to="/my-journals" component={Link}>
          My Journals
        </Button>
        <Button to="/popular" component={Link}>
          Popular Journals
        </Button>
        <Button to="/shared-with-me" component={Link}>
          Journals Shared With Me
        </Button>
      </Stack>
      <div>
        <Avatar />
      </div>
    </Stack>
  );
};

export default Header;
