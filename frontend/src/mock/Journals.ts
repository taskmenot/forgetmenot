import Authors from "./Authors";
import Entries from "./Entries";

const Journals: Journal[] = [
  {
    title: "Daily Journal",
    entries: Entries,
    isPrivate: false,
    author: Authors[0],
    color: "#023047",
  },
  { title: "Diary", entries: [], isPrivate: true, author: Authors[0] },
];

export default Journals;
