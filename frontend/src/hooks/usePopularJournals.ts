import { useState } from "react";
import Journals from "../mock/Journals";

const usePopularJournals = (): {
  journals: Journal[];
  setJournals: (journals: Journal[]) => void;
} => {
  const [journals, setJournals] = useState<Journal[]>(
    Journals.filter((journal) => !journal.isPrivate)
  );

  return { journals, setJournals };
};

export default usePopularJournals;
