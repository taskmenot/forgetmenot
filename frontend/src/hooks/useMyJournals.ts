import { useState } from "react";
import Journals from "../mock/Journals";

const useMyJournals = (): {
  journals: Journal[];
  setJournals: (journals: Journal[]) => void;
} => {
  const [journals, setJournals] = useState<Journal[]>(Journals);
  return { journals, setJournals };
};

export default useMyJournals;
