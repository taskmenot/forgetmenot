import { Stack } from "@mui/material";
import React from "react";
import Header from "../components/layout/Header";

interface SharedProps {}
const Shared: React.FC<SharedProps> = () => {
  return (
    <div>
      <Header />
      <Stack direction="row" padding="1rem" gap="1rem"></Stack>
    </div>
  );
};

export default Shared;
