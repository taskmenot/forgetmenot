import { Stack } from "@mui/material";
import React from "react";
import Header from "../components/layout/Header";

interface HomeProps {}
const Home: React.FC<HomeProps> = () => {
  return (
    <div>
      <Header />
      <Stack direction="row" padding="1rem" gap="1rem"></Stack>
    </div>
  );
};

export default Home;
