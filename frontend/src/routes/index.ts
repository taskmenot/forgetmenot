export { default as Home } from "./Home";
export { default as MyJournals } from "./MyJournals";
export { default as Popular } from "./Popular";
export { default as Shared } from "./Shared";
