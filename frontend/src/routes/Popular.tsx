import { Paper, Stack, Typography } from "@mui/material";
import React from "react";
import Header from "../components/layout/Header";
import usePopularJournals from "../hooks/usePopularJournals";

interface PopularProps {}
const Popular: React.FC<PopularProps> = () => {
  const { journals } = usePopularJournals();

  return (
    <div>
      <Header />
      <Stack direction="column" padding="1rem">
        {journals?.map((journal, index) => (
          <Paper
            key={`journal-${journal.title}-${index}`}
            elevation={3}
            sx={{
              background: journal.color || "white",
              padding: "1rem",
              width: "15rem",
              height: "22rem",
              transition: "transform 1s",
              "&:hover": { cursor: "pointer" },
            }}
            onClick={(e: React.MouseEvent<HTMLDivElement>) => {
              e.currentTarget.style.transform = "rotateY(180deg)";
              console.log(`clicked journal ${journal.title}`);
            }}
          >
            <Stack
              width="15rem"
              height="22rem"
              direction="column"
              justifyContent="space-between"
              alignItems="start"
            >
              <Typography variant="h4" color="white">
                {journal.title}
              </Typography>
              <Typography variant="h6" color="white">
                By: {journal.author.firstName} {journal.author.lastName}
              </Typography>
            </Stack>
          </Paper>
        ))}
      </Stack>
    </div>
  );
};

export default Popular;
