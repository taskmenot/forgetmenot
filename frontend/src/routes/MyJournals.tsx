import {
  Add,
  Create,
  ExpandLess,
  ExpandMore,
  MoreVert,
  StarBorder,
  LibraryBooks,
} from "@mui/icons-material";
import {
  Button,
  Collapse,
  IconButton,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Paper,
  Stack,
  Tab,
  Tabs,
} from "@mui/material";
import React, { useState } from "react";
import Editor from "../components/core/Editor";
import Header from "../components/layout/Header";
import useMyJournals from "../hooks/useMyJournals";

const newEntry = {
  title: "",
  content: "",
};

interface MyJournalsProps {}
const MyJournals: React.FC<MyJournalsProps> = () => {
  const { journals, setJournals } = useMyJournals();
  const [open, setOpen] = useState<string>("new");
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const menuOpen = Boolean(anchorEl);
  const [currentJournal, setCurrentJournal] = useState<Journal>();
  const [currentEntry, setCurrentEntry] = useState<Entry>(newEntry);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleMenuAddEntry = (journal: Journal) => {
    const updatedJournals = [...journals];
    const updatedJournal = updatedJournals.find((j) => journal === j);
    updatedJournal?.entries.push({
      title: "New Entry",
      content: "",
    });
    setJournals(updatedJournals);
    setAnchorEl(null);
  };
  return (
    <div>
      <Header />
      <Stack direction="row" padding="1rem" gap="1rem">
        <Stack direction="column" spacing={2} width="25%">
          <Paper elevation={4}>
            <List component="nav">
              {journals.map((journal, index) => (
                <div key={`journal-${journal.title}`}>
                  <ListItemButton
                    title={journal.title}
                    onClick={(
                      e: React.MouseEvent<HTMLDivElement, MouseEvent>
                    ) => {
                      const currentJournalTitle = e.currentTarget?.title;
                      setOpen(
                        currentJournalTitle === open ? "" : currentJournalTitle
                      );
                      setCurrentJournal(journal);
                    }}
                  >
                    <ListItemIcon>
                      {journal.icon || <LibraryBooks />}
                    </ListItemIcon>
                    <ListItemText primary={journal.title} />
                    <IconButton
                      onClick={(
                        e: React.MouseEvent<HTMLButtonElement, MouseEvent>
                      ) => {
                        e.stopPropagation();
                        const updatedJournals = [...journals];
                        const updatedJournal = updatedJournals.find(
                          (j) => journal === j
                        );
                        updatedJournal?.entries.push({
                          title: "New Entry",
                          content: "",
                        });
                        setJournals(updatedJournals);
                      }}
                    >
                      <Create />
                    </IconButton>
                    {journal.entries?.length > 0 &&
                      (open === journal.title ? (
                        <ExpandLess />
                      ) : (
                        <ExpandMore />
                      ))}
                    <IconButton
                      aria-controls={open ? "basic-menu" : undefined}
                      aria-haspopup="true"
                      aria-expanded={open ? "true" : undefined}
                      onClick={handleClick}
                    >
                      <MoreVert />
                    </IconButton>
                    <Menu
                      id="basic-menu"
                      anchorEl={anchorEl}
                      open={menuOpen}
                      onClose={handleMenuClose}
                      MenuListProps={{
                        "aria-labelledby": `journal-more-${journal.title}-${index}`,
                      }}
                    >
                      <MenuItem
                        onClick={(e: React.MouseEvent<HTMLLIElement>) => {
                          e.stopPropagation();
                          handleMenuAddEntry(journal);
                        }}
                      >
                        Add Entry
                      </MenuItem>
                      <MenuItem onClick={handleMenuClose}>Remove</MenuItem>
                    </Menu>
                  </ListItemButton>
                  {journal.entries?.length > 0 && (
                    <Collapse
                      in={open === journal.title}
                      timeout="auto"
                      unmountOnExit
                    >
                      <List component="div" disablePadding>
                        {journal.entries.map((entry) => (
                          <ListItemButton
                            sx={{ pl: 4 }}
                            key={`journal-entry-${entry.title}`}
                            onClick={() => setCurrentEntry(entry)}
                          >
                            <ListItemIcon>
                              <StarBorder />
                            </ListItemIcon>
                            <ListItemText primary={entry.title} />
                          </ListItemButton>
                        ))}
                      </List>
                    </Collapse>
                  )}
                </div>
              ))}
            </List>
            <Stack direction="row" justifyContent="center" padding="0.1rem">
              <Button
                startIcon={<Add />}
                variant="text"
                onClick={() =>
                  setJournals([
                    ...journals,
                    {
                      title: "New Journal",
                      entries: [],
                      isPrivate: true,
                      author: { firstName: "Smokey", lastName: "Bear" },
                    },
                  ])
                }
              >
                Add New Journal
              </Button>
            </Stack>
          </Paper>
        </Stack>
        <Stack direction="column" width="75%">
          <Editor entry={currentEntry} setEntry={setCurrentEntry} />
        </Stack>
      </Stack>
    </div>
  );
};

export default MyJournals;
